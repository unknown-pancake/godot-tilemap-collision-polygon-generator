Godot Tilemap Collision Polygon Generator
=========================================

Generates collision polygons based on the currently selected tilemap.

The algorithm detects islands and generates one for each island found. Does not support hollow islands (ie with a hole inside). The addon generates the collision polygon nodes as sibblings of the tilemap (and thus doesn't work if the tilemap is the root of the scene).

Usage
-----

1. Enable the addon.
2. Select a tilemap.
3. Click on "Gen. Coll. Poly." on the right of the "TileMap" button, in the toolbar.
4. Done.
