tool
extends EditorPlugin



const Generator = preload("./generator.gd")



var button: Button



func _exit_tree():
	if button != null:
		button.queue_free()
		button = null



func edit(obj: Object) -> void:
	if button: 
		button.queue_free()
		button = null
	
	button = Button.new()
	button.text = "Gen. Coll. Poly."
	button.connect("pressed", self, "_generate_collision_polygons", [obj])
	add_control_to_container(EditorPlugin.CONTAINER_CANVAS_EDITOR_MENU, button)

func handles(obj: Object) -> bool:
	var handle = obj is TileMap
	
	if not handle and button != null:
		button.queue_free()
		button = null
	
	return handle



func _generate_collision_polygons(tilemap: TileMap) -> void:
	var generator = Generator.new()
	generator.tilemap = tilemap
	generator.generate()
