extends Reference


var tilemap: TileMap

var start_position: Vector2
var current_position: Vector2
var current_direction: Vector2

var island_mapping: Dictionary
var island_count: int
var island_vertices: Dictionary
var current_island: int



func generate() -> void:
	island_count = 0
	island_mapping = {}
	island_vertices = {}
	current_island = 0
	
	var st = OS.get_ticks_msec()
	
	map_isles()
	walk_around_isles()
	generate_collision_polygons()
	
	print("Generated collision polygons in ", OS.get_ticks_msec() - st, "ms.")



func map_isles() -> void:
	for pos in tilemap.get_used_cells():
		flood_island_mapping(pos)

func flood_island_mapping(pos):
	if island_mapping.has(pos):
		return
	
	island_count += 1
	island_vertices[island_count] = []
	
	var queue = []
	
	island_mapping[pos] = island_count
	queue.push_back(pos)
	
	while not queue.empty():
		var p = queue.pop_front()
		
		var pp = p + Vector2.LEFT
		if tilemap.get_cellv(pp) != TileMap.INVALID_CELL and not island_mapping.has(pp):
			island_mapping[pp] = island_count
			queue.push_back(pp)
		
		pp = p + Vector2.RIGHT
		if tilemap.get_cellv(pp) != TileMap.INVALID_CELL and not island_mapping.has(pp):
			island_mapping[pp] = island_count
			queue.push_back(pp)
		
		pp = p + Vector2.DOWN
		if tilemap.get_cellv(pp) != TileMap.INVALID_CELL and not island_mapping.has(pp):
			island_mapping[pp] = island_count
			queue.push_back(pp)
		
		pp = p + Vector2.UP
		if tilemap.get_cellv(pp) != TileMap.INVALID_CELL and not island_mapping.has(pp):
			island_mapping[pp] = island_count
			queue.push_back(pp)
	



func walk_around_isles() -> void:
	var isles_done = {}
	
	for pos in tilemap.get_used_cells():
		var island = island_mapping[pos]
		
		if not isles_done.has(island):
			current_island = island
			walk_around(find_start_position(pos))
			isles_done[island] = true

func find_start_position(pos: Vector2) -> Vector2:
	while true:
		if tilemap.get_cell(pos.x - 1, pos.y) == TileMap.INVALID_CELL:
			break
		
		pos.x -= 1
	
	while true:
		if tilemap.get_cell(pos.x, pos.y - 1) == TileMap.INVALID_CELL:
			break
		
		pos.y -= 1
	
	island_vertices[current_island].push_back(pos * tilemap.cell_size)
	return pos

func walk_around(pos: Vector2) -> void:
	start_position = pos
	current_position = pos
	current_direction = Vector2.RIGHT
	
	walk()
	while not (current_position == start_position and current_direction == Vector2.RIGHT):
		walk()

func walk() -> void:
	while true:
		if current_direction == Vector2.RIGHT:
			if has_cell_off(0, -1):
				add_vertex(0, 0)
				current_direction = Vector2.UP
				current_position.y -= 1
				return
			
			if not has_cell_off(1, 0):
				add_vertex(1, 0)
				current_direction = Vector2.DOWN
				return
		
		if current_direction == Vector2.DOWN:
			if has_cell_off(1, 0):
				add_vertex(1, 0)
				current_direction = Vector2.RIGHT
				current_position.x += 1
				return
			
			if not has_cell_off(0, 1):
				add_vertex(1, 1)
				current_direction = Vector2.LEFT
				return
		
		if current_direction == Vector2.LEFT:
			if has_cell_off(0, 1):
				add_vertex(0, 1)
				current_direction = Vector2.DOWN
				current_position.y += 1
				return
			
			if not has_cell_off(-1, 0):
				add_vertex(0, 1)
				current_direction = Vector2.UP
				return
		
		if current_direction == Vector2.UP:
			if has_cell_off(-1, 0):
				add_vertex(0, 1)
				current_direction = Vector2.LEFT
				current_position.x -= 1
				return
			
			if not has_cell_off(0, -1):
				add_vertex(0, 0)
				current_direction = Vector2.RIGHT
				return
		
		current_position += current_direction


func generate_collision_polygons() -> void:
	var count = 0
	for polygon in island_vertices.values():
		var n = tilemap.name + "_shape_" + str(count)
		var cp: CollisionPolygon2D
		
		if tilemap.get_parent().has_node(n):
			cp = tilemap.get_parent().get_node(n)
		else:
			cp = CollisionPolygon2D.new()
			tilemap.get_parent().add_child(cp)
			cp.owner = tilemap.owner
			cp.name = n
		
		cp.polygon = polygon
		count += 1


func has_cell_off(x: int, y: int) -> bool:
	return tilemap.get_cell(current_position.x + x, current_position.y + y) != TileMap.INVALID_CELL

func add_vertex(x: int, y: int) -> void:
	island_vertices[current_island].push_back((current_position + Vector2(x, y)) * tilemap.cell_size)
